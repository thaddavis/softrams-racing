// This file is required by karma.conf.js and loads recursively all the .spec and framework files

import 'zone.js/dist/zone-testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from '@angular/platform-browser-dynamic/testing';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

import { AppService } from './app/app.service';
import { AuthService } from './app/auth/auth.service';

import { AppComponent } from './app/app.component';
import { BannerComponent } from './app/banner/banner.component';
import { MemberDetailsComponent } from './app/member-details/member-details.component';
import { MembersComponent } from './app/members/members.component';
import { LoginComponent } from './app/login/login.component';
import { AddMemberComponent } from './app/add-member/add-member.component';
import { EditMemberComponent } from './app/edit-member/edit-member.component';

import {
  AuthGuardService as AuthGuard
} from './app/auth/auth-guard.service';

declare const require: any;
const ROUTES = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'members',
    component: MembersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'add-member',
    component: AddMemberComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'edit-member/:id',
    component: EditMemberComponent,
    canActivate: [AuthGuard]
  }
];

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting()
);

beforeEach(() => {
  TestBed.configureTestingModule({
    declarations: [AppComponent, BannerComponent, MemberDetailsComponent, MembersComponent, LoginComponent, AddMemberComponent, EditMemberComponent],
    imports: [
      BrowserModule,
      RouterModule.forRoot(ROUTES, { useHash: true }),
      ReactiveFormsModule,
      FormsModule,
      HttpClientModule
    ],
    providers: [AppService, AuthService, AuthGuard, HttpClient],
    // imports: [ReactiveFormsModule],
    // declarations: [YourComponent, ...],
    // providers: [...]

  });
});

// Then we find all the tests.
const context = require.context('./', true, /\.spec\.ts$/);
// And load the modules.
context.keys().map(context);
