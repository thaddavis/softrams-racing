import { AbstractControl } from '@angular/forms';

export function TeamNameValidator(control: AbstractControl) {
  if (!control.value) {
    return { empty: true };
  }
  return null;
}