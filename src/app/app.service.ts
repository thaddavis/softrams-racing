import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import get from 'lodash/get';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  api = 'http://localhost:8000/api';
  username: string;

  constructor(private http: HttpClient) { }

  // Returns all members
  getMembers() {
    return this.http
      .get(`${this.api}/members`)
      .pipe(catchError(this.handleError));
  }

  // Returns a member
  getMember(id: string) {
    return this.http
      .get(`${this.api}/member/${id}`)
      .pipe(catchError(this.handleError));
  }

  // Deletes a member
  deleteMember(id: string) {
    return this.http
      .delete(`${this.api}/member/${id}`)
      .pipe(catchError(this.handleError));
  }

  setUsername(name: string): void {
    this.username = name;
  }

  // Adds a member
  addMember(memberForm) {
    return this.http
      .post(`${this.api}/addMember`, memberForm)
      .pipe(catchError(this.handleError));
  }

  editMember(memberid: string, memberForm: object) {
    return this.http
      .put(`${this.api}/editMember/${memberid}`, memberForm)
      .pipe(catchError(this.handleError));
  }

  getTeams() {
    return this.http
      .get(`${this.api}/teams`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return [];
  }
}
