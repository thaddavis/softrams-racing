export default interface Team {
  id: string;
  teamName: string;
}