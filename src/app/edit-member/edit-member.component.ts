import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import get from 'lodash/get';
import { mergeMap } from 'rxjs/operators';
import Team from '../models/team';
import Member from '../models/member';
import { TeamNameValidator } from '../validators/teamName.validator';
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-edit-member',
  templateUrl: './edit-member.component.html',
  styleUrls: ['./edit-member.component.css']
})
export class EditMemberComponent implements OnInit {

  member: Member;
  memberid: string = '';
  teams: Team[];
  editMemberForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public appService: AppService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.memberid = this.route.snapshot.paramMap.get('id');

    this.appService.getTeams()
      .pipe(mergeMap((teams) => {
        this.teams = teams;
        return this.appService.getMember(this.memberid);
      }))
      .subscribe((member: Member) => {
        let memberObject: Member = null;
        if (Array.isArray(this.teams) && this.teams.length > 0) {
          // memberObject = this.teams.find((team: Team) => {
          //   console.log('team', team.teamName);
          //   return team.teamName === member.team
          // }) || null;
          for (let i = 0; i < this.teams.length; i++) {
            const team: Team = this.teams[i];
            if (team.teamName === member.team) {
              memberObject = member;
              break;
            }
          }

        } else {
          memberObject = null
        }

        let firstName: string = (member && member.firstName || '');
        let lastName: string = (member && member.lastName || '');
        let jobTitle: string = (member && member.jobTitle || '');
        let status: boolean = (member && member.status === 'Active' ? true : false);
        let team: string = (member && memberObject && memberObject.team);

        this.editMemberForm = this.fb.group({
          firstName: new FormControl(firstName, Validators.required),
          lastName: new FormControl(lastName, Validators.required),
          jobTitle: new FormControl(jobTitle, Validators.required),
          status: new FormControl(status, Validators.nullValidator),
          team: new FormControl(team, [Validators.required, TeamNameValidator]),
        });
      },
        (e) => {
          console.error(e);

        });
  }

  submit() {
    if (this.editMemberForm.value) this.editMemberForm.value.status = get(this.editMemberForm, 'value.status') ? 'Active' : 'Inactive';
    this.appService.editMember(this.memberid, this.editMemberForm.value).subscribe(() => {
      this.router.navigate(['/members']);
    },
      (e) => {
        console.error(e);
      });
  }

  cancel() {
    this.router.navigate(['/members']);
  }

}
