// src/app/auth/auth.service.ts
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  constructor() { }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('username');
    // true or false
    return token ? true : false;
  }
}