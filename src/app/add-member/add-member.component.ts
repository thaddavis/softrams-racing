import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import get from 'lodash/get';

import { TeamNameValidator } from '../validators/teamName.validator';

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.css']
})
export class AddMemberComponent implements OnInit {
  teams: object[];
  addMemberForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private appService: AppService) { }

  ngOnInit() {
    this.appService.getTeams().subscribe((teams) => {
      this.teams = teams;
    });

    this.addMemberForm = this.fb.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      jobTitle: new FormControl('', Validators.required),
      status: new FormControl('', Validators.nullValidator),
      team: new FormControl('', [Validators.required, TeamNameValidator]),
    });
  }

  submit() {
    if (this.addMemberForm.value) this.addMemberForm.value.status = get(this.addMemberForm, 'value.status') ? 'Active' : 'Inactive';

    this.appService.addMember(this.addMemberForm.value).subscribe(() => {
      this.router.navigate(['/members']);
    },
      (e) => {
        console.error(e);
      });
  }

  cancel() {
    this.router.navigate(['/members']);
  }
}
