import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];

  constructor(public appService: AppService, private router: Router) { }

  ngOnInit() {
    this.appService.getMembers().subscribe(members => (this.members = members));
  }

  goToAddMemberForm() {
    this.router.navigate(['/add-member']);
  }

  editMemberByID(id: number) {
    this.router.navigate(['/edit-member', id]);
  }

  deleteMemberById(id: string) {
    this.appService.deleteMember(id)
      .pipe(mergeMap((res) => {
        return this.appService.getMembers();
      }))
      .subscribe(
        (members) => {
          this.members = members;
        },
        (e) => {
          console.error(e);
          this.members = [];
        }
      );
  }
}
