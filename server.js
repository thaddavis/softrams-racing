const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var hsts = require('hsts');
const path = require('path');
var xssFilter = require('x-xss-protection');
var nosniff = require('dont-sniff-mimetype');
const request = require('request');

const app = express();

app.use(cors());
app.use(express.static('assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');
app.use(xssFilter());
app.use(nosniff());
app.set('etag', false);
app.use(
  helmet({
    noCache: true
  })
);
app.use(
  hsts({
    maxAge: 15552000 // 180 days in seconds
  })
);

app.use(
  express.static(path.join(__dirname, 'dist/softrams-racing'), {
    etag: false
  })
);

app.get('/api/members', (req, res) => {
  request('http://localhost:3000/members', (err, response, body) => {
    if (err) res.status(500).send(err.toString());
  
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

app.get('/api/member/:id', (req, res) => {
  try {
    if (!req.params.id) throw 'id_not_provided';
    const memberid = req.params.id;
    request(`http://localhost:3000/members/${memberid}`, (err, response, body) => {
      if (response.statusCode <= 500) {
        res.send(body);
      }
    });
  } catch (e) {
    console.log(e);
    res.status(500).send(e.toString());
  }

});

app.delete('/api/member/:id', (req, res) => {
  try {
    if (!req.params.id) throw 'id_not_provided';
    const memberid = req.params.id;
    request.delete(`http://localhost:3000/members/${memberid}`, (err, response, body) => {
      if (response.statusCode <= 500) {
        console.log('bbb', body);
        res.send(body);
      }
    });
  } catch (e) {
    console.log(e);
    res.status(500).send(e.toString());
  }

});

// TODO: Dropdown!
app.get('/api/teams', (req, res) => {
  request('http://localhost:3000/teams', (err, response, body) => {
    if (err) res.status(500).send(err.toString());  

    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

// Submit Form!
app.post('/api/addMember', (req, res) => {
  try {
    if (!req.body.firstName) throw 'firstName_not_provided';
    if (!req.body.lastName) throw 'lastName_not_provided';
    if (!req.body.jobTitle) throw 'jobTitle_not_provided';
    if (!(req.body.status === 'Active' || req.body.status === 'Inactive')) throw 'status_not_provided';
    if (!req.body.team) throw 'team_not_provided';

    request.post('http://localhost:3000/members', {
        form: req.body
      }, (err, response, body) => {
        if (err) {
          res.status(500).send(err.toString());
        }

        if (response.statusCode <= 500) {
          console.log("bbb", body);
          res.send(body);
        }
      });
  } catch (e) {
    res.status(500).send(e.toString());
  }
});

app.put('/api/editMember/:id', (req, res) => {
  try {
    if (!req.params.id) throw 'id_not_provided';
    if (!req.body.firstName) throw 'firstName_not_provided';
    if (!req.body.lastName) throw 'lastName_not_provided';
    if (!req.body.jobTitle) throw 'jobTitle_not_provided';
    if (!(req.body.status === 'Active' || req.body.status === 'Inactive')) throw 'status_not_provided';
    if (!req.body.team) throw 'team_not_provided';

    const memberid = req.params.id;

    request.put(`http://localhost:3000/members/${memberid}`, {
        form: req.body
      }, (err, response, body) => {
        if (err) res.status(500).send(err.toString());

        if (response.statusCode <= 500) {
          res.send(body);
        }
      });
  } catch (e) {
    console.log(e);
    res.status(500).send(e.toString());
  }
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/softrams-racing/index.html'));
});

app.listen('8000', () => {
  console.log('Vrrrum Vrrrum! Server starting!');
});
